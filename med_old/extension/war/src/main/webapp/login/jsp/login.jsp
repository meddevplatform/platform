<%--

    Copyright (C) 2009 eXo Platform SAS.
    
    This is free software; you can redistribute it and/or modify it
    under the terms of the GNU Lesser General Public License as
    published by the Free Software Foundation; either version 2.1 of
    the License, or (at your option) any later version.
    
    This software is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
    Lesser General Public License for more details.
    
    You should have received a copy of the GNU Lesser General Public
    License along with this software; if not, write to the Free
    Software Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
    02110-1301 USA, or see the FSF site: http://www.fsf.org.

--%>

<%@ page import="java.net.URLEncoder"%>
<%@ page import="javax.servlet.http.Cookie"%>
<%@ page import="org.exoplatform.container.PortalContainer"%>
<%@ page
	import="org.exoplatform.services.resources.ResourceBundleService"%>
<%@ page import="java.util.ResourceBundle"%>
<%@ page import="org.gatein.common.text.EntityEncoder"%>
<%@ page language="java"%>
<%
	String contextPath = request.getContextPath();

	String username = request.getParameter("username");
	if (username == null) {
		username = "";
	} else {
		EntityEncoder encoder = EntityEncoder.FULL;
		username = encoder.encode(username);
	}

	ResourceBundleService service = (ResourceBundleService) PortalContainer
			.getCurrentInstance(session.getServletContext())
			.getComponentInstanceOfType(ResourceBundleService.class);
	ResourceBundle res = service
			.getResourceBundle(service.getSharedResourceBundleNames(),
					request.getLocale());

	Cookie cookie = new Cookie(
			org.exoplatform.web.login.LoginServlet.COOKIE_NAME, "");
	cookie.setPath(request.getContextPath());
	cookie.setMaxAge(0);
	response.addCookie(cookie);

	String uri = (String) request
			.getAttribute("org.gatein.portal.login.initial_uri");
	boolean error = request
			.getAttribute("org.gatein.portal.login.error") != null;

	response.setCharacterEncoding("UTF-8");
	response.setContentType("text/html; charset=UTF-8");
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>Login</title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<script src="/med-extension/js/jquery/1.11.1/jquery.min.js"></script>
<script src="/med-extension/js/bootstrap/3.2.0/bootstrap.min.js"></script>
<script type="text/javascript">
	$(document).ready(
			function() {
				var divContent = jQuery("#platformInfoDiv");
				var requestJsonPlatformInfo = jQuery.ajax({
					type : "GET",
					url : "/portal/rest/platform/info",
					async : false,
					dataType : 'json'
				});
				if (requestJsonPlatformInfo.readyState == 4
						&& requestJsonPlatformInfo.status == 200) {
					//readyState 4: request finished and response is ready
					//status 200: "OK"
					var myresponseText = requestJsonPlatformInfo.responseText;
					var jsonPlatformInfo = jQuery.parseJSON(myresponseText);
					htmlContent += "v"
					htmlContent += jsonPlatformInfo.platformVersion;
					htmlContent += " - build "
					htmlContent += jsonPlatformInfo.platformBuildNumber;
				} else {
					htmlContent += "4.0"
				}
				divContent.text(htmlContent);
			});
</script>

<!-- Bootstrap core CSS -->
<link href="/med-extension/css/bootstrap.min.css" rel="stylesheet" />

</head>



<body>
	<%
		if (error) {
	%>
	<div class="signinFail">
		<i class="uiIconError"></i><%=res.getString("portal.login.SigninFail")%></div>
	<%
		}
	%>

	<div class="navbar navbar-inverse navbar-fixed-top" role="navigation">
		<div class="container">
			<div class="navbar-header">
				<button type="button" class="navbar-toggle collapsed"
					data-toggle="collapse" data-target=".navbar-collapse">
					<span class="sr-only">Toggle navigation</span> <span
						class="icon-bar"></span> <span class="icon-bar"></span> <span
						class="icon-bar"></span>
				</button>
				<a class="navbar-brand" href="#">MED SOCIAL</a>
			</div>
			<div class="navbar-collapse collapse">
				<form name="loginForm" action="<%=contextPath + "/login"%>"
					method="post" class="navbar-form navbar-right" role="form">
					<%
						if (uri != null) {
							uri = EntityEncoder.FULL.encode(uri);
					%>
					<input type="hidden" name="initialURI" value="<%=uri%>" />
					<%
						}
					%>

					<div class="form-group">
						<input class="username" tabindex="1" id="username" name="username"
							type="text"
							placeholder="<%=res.getString("portal.login.Username")%>"
							onblur="this.placeholder = '<%=res.getString("portal.login.Username.blur")%>'"
							onfocus="this.placeholder = ''" />
					</div>

					<div class="form-group">
						<input class="password" tabindex="2" id="UIPortalLoginFormControl"
							type="password" id="password" name="password"
							placeholder="<%=res.getString("portal.login.Password")%>"
							onblur="this.placeholder = '<%=res.getString("portal.login.Password")%>'"
							onfocus="this.placeholder = ''" />
					</div>

					<button class="btn btn-success" onclick="login();"><%=res.getString("portal.login.Signin")%></button>
					<a class="btn btn-success" href="/portal/med/cadastro">Registro</a>

					<script type='text/javascript'>
						function login() {
							document.loginForm.submit();
						}
					</script>
				</form>
			</div>
		</div>
	</div>

	<div class="jumbotron">
		<div class="container">
			<h3>Relacionamentos, Facilidades e tudo que voc� precisa para
				sua carreira!</h3>
			<p>Aproxime-se das pessoas que podem fazer a diferen�a.</p>
		</div>
	</div>
	<div class="learn-more">
		<div class="container">
			<div class="row">
				<div class="col-md-4">
					<h3>Grupos</h3>
					<p>A definir ...</p>
					<p>
						<a href="#">Sites relacionados</a>
					</p>
				</div>
				<div class="col-md-4">
					<h3>Parceiros</h3>
					<p>A definir ...</p>
					<p>
						<a href="#">Sites relacionados</a>
					</p>
				</div>
				<div class="col-md-4">
					<h3>Valores</h3>
					<p>A definir ...</p>
				</div>
			</div>
		</div>
	</div>
</body>
</html>
