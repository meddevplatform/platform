package br.com.med.common.services;

import java.io.IOException;
import java.util.Iterator;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;

public class CrmValidator {
	public static String EMPTY_SPACES= "\\s+";
	public static int MININUM_NAME_LENGHT= 8;


	public  boolean isCRMValid(String crm, String name) {
		Document doc = null;
		boolean found = false;
		
		if(crm==null || name == null || name.length() < MININUM_NAME_LENGHT || crm.trim().length() == 0){
			return false;
		}

		name = name.replaceAll(EMPTY_SPACES, "+");
		
		try {
			doc = Jsoup.connect("http://portal.cfm.org.br/index.php?medicosNome="+name+"&medicosCRM="+crm+"&medicosUF=&medicosSituacao=&medicosTipoInscricao=&medicosEspecialidade=&buscaEfetuada=true&option=com_medicos#buscaMedicos").get();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		found = parseResponse(doc);
		return found;
	}

	private boolean parseResponse(Document doc) {
		boolean isFound = false;
		Elements newsHeadlines = doc.getElementsByClass("regrow");
		Iterator<org.jsoup.nodes.Element> it = newsHeadlines.iterator();
		for (;it.hasNext();){
			org.jsoup.nodes.Element el = it.next();
			Elements nomes = el.getElementsByTag("span");
			Iterator<org.jsoup.nodes.Element> itNomes = nomes.iterator();
			for (;itNomes.hasNext();){
				org.jsoup.nodes.Element elNome = itNomes.next();
				System.out.println(elNome.text());
				if(elNome.getElementsContainingText("Ativo").size() >0){
					isFound = true;
				}
			} 
		}
		return isFound;
	}
}
