package br.com.med.common.enumeration;

public enum AcademicAttribute{
	UNIVERSITY_NAME ("user.academic.university.name", "University name"),
	STUDENT_ID ("user.academic.student.id", "Identification of a student on University"),
	STUDENT_AMBASSOR ("user.academic.student.ambassor", "Ambassor that has invited");
	
	private String key;
	private String attributeDescription;
	
	AcademicAttribute(String key, String attributeDescription){
		this.key = key;
		this.attributeDescription = attributeDescription;
	}
	
	public String getKey(){
		return key;
	}
	
	public String getAttributeDescription(){
		return attributeDescription;
	}
}
