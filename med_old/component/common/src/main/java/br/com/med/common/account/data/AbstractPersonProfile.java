package br.com.med.common.account.data;

import br.com.med.common.enumeration.MedGroup;
import br.com.med.common.enumeration.MedMembership;

public abstract class AbstractPersonProfile extends AbstractProfile{
	
	// Another attributes are available on User.java!
	public enum PersonAttribute{
		FIRST_NAME (PERSONAL_INFO_KEYS[0], "Given name"),
		LAST_NAME (PERSONAL_INFO_KEYS[1], "Last name"),
		BIRTH_DATE (PERSONAL_INFO_KEYS[3], "Birth date"),
		GENDER (PERSONAL_INFO_KEYS[4], "Gender: Male or Female"),
		ADDRESS_ST_AVENUE (HOME_INFO_KEYS[1], "Gender: Male or Female"),
		ADDRESS_CITY (HOME_INFO_KEYS[2], "Address city"),
		ADDRESS_STATE (HOME_INFO_KEYS[3], "Address State/Province"),
		ADDRESS_ZIP_CODE (HOME_INFO_KEYS[2], "Address ZIP Code");
		
		private String key;
		private String attributeDescription;
		
		PersonAttribute(String key, String attributeDescription){
			this.key = key;
			this.attributeDescription = attributeDescription;
		}
		
		public String getKey(){
			return key;
		}
		
		public String getAttributeDescription(){
			return attributeDescription;
		}
	}
	
	public AbstractPersonProfile(String userName, MedMembership defaultMembershipType, MedGroup defaultGroup){
		super(userName, Type.SINGLE_PERSON, defaultMembershipType, defaultGroup);
	}
	
	public void setFirstName(String firstName){
		setAttribute(PersonAttribute.FIRST_NAME.getKey(), firstName);
	}
	
	public String getFirstName(){
		return getAttribute(PersonAttribute.FIRST_NAME.getKey());
	}
	
	public void setLastName(String lastName){
		setAttribute(PersonAttribute.LAST_NAME.getKey(), lastName);
	}
	
	public String getLastName(){
		return getAttribute(PersonAttribute.LAST_NAME.getKey());
	}

	public void setGender(String gender){
		setAttribute(PersonAttribute.GENDER.getKey(), gender);
	}
	
	public String getGender(){
		return getAttribute(PersonAttribute.GENDER.getKey());
	}
	
	public void setBirthDate(String birthDate){
		setAttribute(PersonAttribute.BIRTH_DATE.getKey(), birthDate);
	}
	
	public String getBirthDate(){
		return getAttribute(PersonAttribute.BIRTH_DATE.getKey());
	}
	
	public void setAddressStreetAvenue(String addressCityAvenue){
		setAttribute(PersonAttribute.ADDRESS_ST_AVENUE.getKey(), addressCityAvenue);
	}
	
	public String getAddressStreetAvenue(){
		return getAttribute(PersonAttribute.ADDRESS_ST_AVENUE.getKey());
	}
	
	public void setAddressCity(String addressCity){
		setAttribute(PersonAttribute.ADDRESS_CITY.getKey(), addressCity);
	}
	
	public String getAddressCity(){
		return getAttribute(PersonAttribute.ADDRESS_CITY.getKey());
	}
	
	public void setAddressState(String addressState){
		setAttribute(PersonAttribute.ADDRESS_STATE.getKey(), addressState);
	}
	
	public String getAddressState(){
		return getAttribute(PersonAttribute.ADDRESS_STATE.getKey());
	}
	
	public void setAddressZipCode(String zipCode){
		setAttribute(PersonAttribute.ADDRESS_ZIP_CODE.getKey(), zipCode);
	}
	
	public String getAddressZipCode(){
		return getAttribute(PersonAttribute.ADDRESS_ZIP_CODE.getKey());
	}
	
}
