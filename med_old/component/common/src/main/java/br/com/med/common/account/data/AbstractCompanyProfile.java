package br.com.med.common.account.data;

import br.com.med.common.enumeration.MedGroup;
import br.com.med.common.enumeration.MedMembership;

public abstract class AbstractCompanyProfile extends AbstractProfile{
	
	// Another attributes are available on User.java!
	public enum CompanyAttribute{
		ADDRESS_ST_AV_NUMBER (HOME_INFO_KEYS[1], "Address Street/Avenue and number"),
		SPECIFIC_BUSINESS ("user.company.business", "Specific business");
		
		private String key;
		private String attributeDescription;
		
		CompanyAttribute(String key, String attributeDescription){
			this.key = key;
			this.attributeDescription = attributeDescription;
		}
		
		public String getKey(){
			return key;
		}
		
		public String getAttributeDescription(){
			return attributeDescription;
		}
	}
	
	public AbstractCompanyProfile(String userName, MedMembership defaultMembershipType, MedGroup defaultGroup){
		super(userName, Type.COMPANY, defaultMembershipType, defaultGroup);
	}

	public void setAddressStreetAvenue(String streetAvenueNumber){
		setAttribute(CompanyAttribute.ADDRESS_ST_AV_NUMBER.getKey(), streetAvenueNumber);
	}
	
	public String getStreetAvenueNumber(){
		return getAttribute(CompanyAttribute.ADDRESS_ST_AV_NUMBER.getKey());
	}
	
	public void setSpecificBusiness(String business){
		setAttribute(CompanyAttribute.SPECIFIC_BUSINESS.getKey(), business);
	}
	
	public String getSpecificBusiness(){
		return getAttribute(CompanyAttribute.SPECIFIC_BUSINESS.getKey());
	}
}
