package br.com.med.common.enumeration;

public enum GroupParent {
	PLATFORM("/platform"),
	BUSINESS("/business");
	
	private String name;
	
	private GroupParent(String name) {
		this.name = name;
	}
	
	public String getName(){
		return name;
	}
}
