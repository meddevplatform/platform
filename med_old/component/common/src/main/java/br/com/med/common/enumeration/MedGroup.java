package br.com.med.common.enumeration;

import java.util.ArrayList;
import java.util.List;

public enum MedGroup {
	DOCTOR("doctor", GroupParent.PLATFORM, "Medico"),
	ACADEMIC("academic", GroupParent.PLATFORM, "Estudante de Medicina"),
	INSTITUTION("institution", GroupParent.BUSINESS, "Clinica / Hospital / Instituição"),
	UNIVERSITY("university", GroupParent.BUSINESS, "Faculdade de Medicina"),
	COMPANY("company", GroupParent.BUSINESS, "Empresa de Insumos e Produtos de Interesse Medico"),
	AGENT("agents", GroupParent.BUSINESS, "Representante de Empresa"),
	MEDICAL_SOCIETY("medical_societies", GroupParent.BUSINESS, "Sociedades Medicas");
	
	private String name;
	private GroupParent parentId;
	private String displayValue;
	
	private MedGroup(String name, GroupParent parentId, String displayValue) {
		this.name = name;
		this.parentId = parentId;
		this.displayValue = displayValue;
	}
	
	public String getName(){
		return name;
	}
	
	public GroupParent getParentId(){
		return parentId;
	}
	
	public String getGroupId(){
		return getParentId().getName() + "/" + getName();
	}
	
	public String getDisplayValue(){
		return displayValue;
	}
	
	public static List<MedGroup> getGroupsByParentId(String parentId){
		List<MedGroup> groups = new ArrayList<MedGroup>(values().length);
		for(MedGroup group : values()){
			if(group.getParentId().equals(parentId)){
				groups.add(group);
			}
		}
		return groups;
	}
}
