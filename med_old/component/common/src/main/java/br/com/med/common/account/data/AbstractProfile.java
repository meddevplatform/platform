package br.com.med.common.account.data;

import org.exoplatform.services.organization.impl.UserProfileImpl;

import br.com.med.common.enumeration.MedGroup;
import br.com.med.common.enumeration.MedMembership;

public abstract class AbstractProfile extends UserProfileImpl{

	public enum Type{
		SINGLE_PERSON,
		COMPANY
	}
	
	public enum CommonAttribute{
		GOVERNAMENTAL_ID ("user.governamental_id", "Governamental identification of a person or company"),
		TYPE ("user.type", "Type of user: single person or company"),
		ADDRESS_CITY (HOME_INFO_KEYS[2], "Address city"),
		ADDRESS_STATE (HOME_INFO_KEYS[3], "Address state"),
		ADDRESS_ZIP_CODE (HOME_INFO_KEYS[4], "Address ZIP code");
		
		private String key;
		private String attributeDescription;
		
		CommonAttribute(String key, String attributeDescription){
			this.key = key;
			this.attributeDescription = attributeDescription;
		}
		
		public String getKey(){
			return key;
		}
		
		public String getAttributeDescription(){
			return attributeDescription;
		}
	}
	
	private MedMembership defaultMembershipType;
	private MedGroup defaultGroup;
	
	public AbstractProfile(String userName, Type type, MedMembership defaultMembershipType, MedGroup defaultGroup){
		super(userName);
		setAttribute(CommonAttribute.TYPE.getKey(), type.toString());
		this.defaultGroup = defaultGroup;
		this.defaultMembershipType = defaultMembershipType;
	}
	
	public Type getType(){
		String typeStr = getAttribute(CommonAttribute.TYPE.getKey());
		return lookupType(typeStr);
	}
	
	public void setGovernamentalId(String governamentalId){
		setAttribute(CommonAttribute.GOVERNAMENTAL_ID.getKey(), governamentalId);
	}
	
	public String getGovernamentalId(){
		return getAttribute(CommonAttribute.GOVERNAMENTAL_ID.getKey());
	}

	
	public void setAddressCity(String city){
		setAttribute(CommonAttribute.ADDRESS_CITY.getKey(), city);
	}
	
	public String getAddressCity(){
		return getAttribute(CommonAttribute.ADDRESS_CITY.getKey());
	}
	
	public void setAddressState(String state){
		setAttribute(CommonAttribute.ADDRESS_STATE.getKey(), state);
	}
	
	public String getAddressState(){
		return getAttribute(CommonAttribute.ADDRESS_STATE.getKey());
	}
	
	public void setAddressZipCode(String zipCode){
		setAttribute(CommonAttribute.ADDRESS_ZIP_CODE.getKey(), zipCode);
	}
	
	public String getAddressZipCode(){
		return getAttribute(CommonAttribute.ADDRESS_ZIP_CODE.getKey());
	}
	
	public MedMembership getDefaultMembershipType() {
		return defaultMembershipType;
	}

	public void setDefaultMembershipType(MedMembership defaultMembershipType) {
		this.defaultMembershipType = defaultMembershipType;
	}

	public MedGroup getDefaultGroup() {
		return defaultGroup;
	}

	public void setDefaultGroupId(MedGroup defaultGroup) {
		this.defaultGroup = defaultGroup;
	}

	public Type lookupType(String typeStr){
		for(Type type : Type.values()){
			if(typeStr.equals(type.toString())){
				return type;
			}
		}
		return null;
	}
}
