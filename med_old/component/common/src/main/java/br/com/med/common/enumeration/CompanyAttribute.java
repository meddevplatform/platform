package br.com.med.common.enumeration;

import static org.exoplatform.services.organization.UserProfile.HOME_INFO_KEYS;

public enum CompanyAttribute{
	ADDRESS_ST_AV_NUMBER (HOME_INFO_KEYS[1], "Address Street/Avenue and number"),
	SPECIFIC_BUSINESS ("user.company.business", "Specific business");
	
	private String key;
	private String attributeDescription;
	
	CompanyAttribute(String key, String attributeDescription){
		this.key = key;
		this.attributeDescription = attributeDescription;
	}
	
	public String getKey(){
		return key;
	}
	
	public String getAttributeDescription(){
		return attributeDescription;
	}
}
