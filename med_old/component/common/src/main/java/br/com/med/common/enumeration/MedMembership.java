package br.com.med.common.enumeration;

public enum MedMembership {
	MANAGER("manager"),
	MEMBER("member"),
	VALIDATOR("validator");
	
	private String name;
	
	private MedMembership(String name) {
		this.name = name;
	}
	
	public String getName(){
		return name;
	}
}
