package br.com.med.common.account.data;

import br.com.med.common.enumeration.MedGroup;
import br.com.med.common.enumeration.MedMembership;

public class AcademicProfile extends AbstractPersonProfile{

	// Another attributes are available on User.java!
	public enum AcademicAttribute{
		UNIVERSITY_NAME ("user.academic.university.name", "University name"),
		STUDENT_ID ("user.academic.student.id", "Identification of a student on University"),
		STUDENT_AMBASSOR ("user.academic.student.ambassor", "Ambassor that has invited");
		
		private String key;
		private String attributeDescription;
		
		AcademicAttribute(String key, String attributeDescription){
			this.key = key;
			this.attributeDescription = attributeDescription;
		}
		
		public String getKey(){
			return key;
		}
		
		public String getAttributeDescription(){
			return attributeDescription;
		}
	}
	
	public AcademicProfile(String userName){
		super(userName, MedMembership.MEMBER, MedGroup.ACADEMIC);
	}
	
	public void setUniversityName(String university){
		setAttribute(AcademicAttribute.UNIVERSITY_NAME.getKey(), university);
	}
	
	public String getUniversityName(){
		return getAttribute(AcademicAttribute.UNIVERSITY_NAME.getKey());
	}
	
	public void setStudentID(String crmState){
		setAttribute(AcademicAttribute.STUDENT_ID.getKey(), crmState);
	}
	
	public String getStudentID(){
		return getAttribute(AcademicAttribute.STUDENT_ID.getKey());
	}
	
	public void setStudentAmbassor(String ambassor){
		setAttribute(AcademicAttribute.STUDENT_AMBASSOR.getKey(), ambassor);
	}
	
	public String getStudentAmbassor(){
		return getAttribute(AcademicAttribute.STUDENT_AMBASSOR.getKey());
	}

}
