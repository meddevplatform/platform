package br.com.med.common.account.data;

import java.io.Serializable;
import java.util.List;

import br.com.med.common.enumeration.Entity;
import br.com.med.common.enumeration.MedGroup;
import br.com.med.common.enumeration.Specialization;

public class Account implements Serializable {

	private static final long serialVersionUID = -5789150983264077895L;
	
	private String username;
	private String password;
	private String confirmPassword;
	
	private String firstName;
	private String lastName;
	private String gender;
	private String email;
	private String birthDate;
	
	private String institutionalId;
	private String institutionalName;
	private String specificBusiness;
	
	private String crm;
	private String ambassor;

	private String governamentalId;
	
	private String universityName;
	private String studentId;
	
	private Address address;
	private MedGroup medGroup;
	private Entity entity;
	private List<Specialization> selectedSpecializations;

	
	public Account() {
		address = new Address();
	}
	
	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getConfirmPassword() {
		return confirmPassword;
	}

	public void setConfirmPassword(String confirmPassword) {
		this.confirmPassword = confirmPassword;
	}

	public String getInstitutionalId() {
		return institutionalId;
	}
	
	public void setInstitutionalId(String institutionalId) {
		this.institutionalId = institutionalId;
	}
	
	public String getInstitutionalName() {
		return institutionalName;
	}

	public void setInstitutionalName(String institutionalName) {
		this.institutionalName = institutionalName;
	}

	public String getSpecificBusiness() {
		return specificBusiness;
	}

	public void setSpecificBusiness(String specificBusiness) {
		this.specificBusiness = specificBusiness;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public String getCrm() {
		return crm;
	}

	public void setCrm(String crm) {
		this.crm = crm;
	}

	public String getAmbassor() {
		return ambassor;
	}

	public void setAmbassor(String ambassor) {
		this.ambassor = ambassor;
	}

	public String getBirthDate() {
		return birthDate;
	}

	public void setBirthDate(String birthDate) {
		this.birthDate = birthDate;
	}

	public String getGovernamentalId() {
		return governamentalId;
	}

	public void setGovernamentalId(String governamentalId) {
		this.governamentalId = governamentalId;
	}

	public String getUniversityName() {
		return universityName;
	}

	public void setUniversityName(String universityName) {
		this.universityName = universityName;
	}

	public String getStudentId() {
		return studentId;
	}

	public void setStudentId(String studentId) {
		this.studentId = studentId;
	}

	public MedGroup getMedGroup() {
		return medGroup;
	}
	
	public void setMedGroup(MedGroup medGroup) {
		this.medGroup = medGroup;
	}
	
	public Address getAddress() {
		return address;
	}
	
	public void setAddress(Address address) {
		this.address = address;
	}
	
	public Entity getEntity() {
		return entity;
	}
	
	public void setEntity(Entity entity) {
		this.entity = entity;
	}
	
	public List<Specialization> getSelectedSpecializations() {
		return selectedSpecializations;
	}
	
	public void setSelectedSpecializations(List<Specialization> selectedSpecializations) {
		this.selectedSpecializations = selectedSpecializations;
	}

	
}
