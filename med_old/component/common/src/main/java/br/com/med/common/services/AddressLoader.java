package br.com.med.common.services;

import java.net.URL;
import java.util.Iterator;

import org.dom4j.Document;
import org.dom4j.Element;
import org.dom4j.io.SAXReader;

import br.com.med.common.account.data.Address;

public class AddressLoader {

	@SuppressWarnings("rawtypes")
	public Address findByZipCode(String zipCode) throws Exception {
		String url = "http://cep.republicavirtual.com.br/web_cep.php?cep=" + zipCode + "&formato=xml";
		
		SAXReader reader = new SAXReader();
		Document document = reader.read(new URL(url));
		
		Element root = document.getRootElement();
		Iterator it = root.elementIterator();
		
		Address address = new Address();
		
		while(it.hasNext()) {
			if(address.isEmpty()){
				address.setEmpty(false);
				address.setZipCode(zipCode);
			}
			
			Element element = (Element) it.next();
			
			if("uf".equals(element.getQualifiedName())) {
				address.setState(element.getText());
			}
			
			if("cidade".equals(element.getQualifiedName())){
				address.setCity(element.getText());
			}
			
			if("logradouro".equals(element.getQualifiedName())) {
				address.setStreetAvenue(element.getText());
			}
			
		}
		return address;
	}


	public static void main(String[] args) {
		AddressLoader loader = new AddressLoader();
		try {
			Address address = loader.findByZipCode("03728005");
			System.out.println(address.toString());
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

}
