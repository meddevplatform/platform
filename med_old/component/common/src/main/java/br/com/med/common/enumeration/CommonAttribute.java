package br.com.med.common.enumeration;

import static org.exoplatform.services.organization.UserProfile.HOME_INFO_KEYS;

public enum CommonAttribute{
	GOVERNAMENTAL_ID ("user.governamental_id", "Governamental identification of a person or company"),
	TYPE ("user.type", "Type of user: single person or company"),
	ADDRESS_CITY (HOME_INFO_KEYS[2], "Address city"),
	ADDRESS_STATE (HOME_INFO_KEYS[3], "Address state"),
	ADDRESS_ZIP_CODE (HOME_INFO_KEYS[4], "Address ZIP code");
	
	private String key;
	private String attributeDescription;
	
	CommonAttribute(String key, String attributeDescription){
		this.key = key;
		this.attributeDescription = attributeDescription;
	}
	
	public String getKey(){
		return key;
	}
	
	public String getAttributeDescription(){
		return attributeDescription;
	}
}
