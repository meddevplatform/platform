package br.com.med.common.enumeration;

public enum Specialization {

	Acupuntura("Acupuntura"),
	AlergiaImulogia("Alergia e Imunologia"),
	Anestesiologia("Anestesiologia"),
	Angiologia("Angiologia"),
	Cancerologia("Cancerologia"),
	Cardiologia("Cardiologia"),
	CirurgiaCardiovascular("Cirurgia Cardiovascular"),
	CirurgiaDaMao("Cirurgia da Mao"),
	CirurgiaDeCabecaEPescoco("Cirurgia de cabeça e pescoço"),
	CirurgiaDoAparelhoDigestivo("Cirurgia do Aparelho Digestivo"),
	CirurgiaGeral("Cirurgia Geral"),
	CirurgiaPediatrica("Cirurgia Pediatrica"),
	CirurgiaPlastica("Cirurgia Plastica"),
	CirurgiaToracica("Cirurgia Torcica"),
	CirurgiaVascular("Cirurgia Vascular"),
	ClinicaMedica("Clínica Médica"),
	Coloproctologia("Coloproctologia"),
	Dermatologia("Dermatologia"),
	EndocrinologiaEMetabologia("Endocrinologia e Metabologia"),
	Endoscopia("Endoscopia"),
	Gastroenterologia("Gastroenterologia"),
	GeneticaMedica("Genetica Medica"),
	Geriatria("Geriatria"),
	GinecologiaEObstetricia("Ginecologia e Obstetricia"),
	HematologiaEHemoterapia("Hematologia e Hemoterapia"),
	Homeopatia("Homeopatia"),
	Infectologia("Infectologia"),
	Mastologia("Mastologia"),
	MedicinaDeFamiliaEComunidade("Medicina de Familia e Comunidade"),
	MedicinaDoTrabalho("Medicina do Trabalho"),
	MedicinaDoTrafego("Medicina do Trafego"),
	MedicinaEsportiva("Medicina Esportiva"),
	MedicinaFisicaReabilitacao("Medicina Física e Reabilitacao"),
	MedicinaIntensiva("Medicina Intensiva"),
	MedicinaLegalPericiaMedica("Medicina Legal e Perícia Medica"),
	MedicinaNuclear("Medicina Nuclear"),
	MedicinaPreventivaSocial("Medicina Preventiva e Social"),
	Nefrologia("Nefrologia"),
	Neurocirurgia("Neurocirurgia"),
	Neurologia("Neurologia"),
	Nutrologia("Nutrologia"),
	Oftalmologia("Oftalmologia"),
	OrtopediaETraumatologia("Ortopedia e Traumatologia"),
	Otorrinolaringologia("Otorrinolaringologia"),
	Patologia("Patologia"),
	PatologiaClinicaMedicinaLaboratorial("Patologia Clinica e Medicina Laboratorial"),
	Pediatria("Pediatria"),
	Pneumologia("Pneumologia"),
	Psiquiatria("Psiquiatria"),
	RadiologiaEDiagnosticoPorImagem("Radiologia e Diagnóstico por Imagem"),
	Radioterapia("Radioterapia"),
	Reumatologia("Reumatologia"),
	Urologia("Urologia");
	
	private String displayValue;
	
	private Specialization(String displayValue) {
		this.displayValue = displayValue;
	}
	
	public String getDisplayValue() {
		return displayValue;
	}
}
