package br.com.med.common.enumeration;

import static org.exoplatform.services.organization.UserProfile.PERSONAL_INFO_KEYS;
import static org.exoplatform.services.organization.UserProfile.HOME_INFO_KEYS;

public enum PersonAttribute{
	FIRST_NAME (PERSONAL_INFO_KEYS[0], "Given name"),
	LAST_NAME (PERSONAL_INFO_KEYS[1], "Last name"),
	BIRTH_DATE (PERSONAL_INFO_KEYS[3], "Birth date"),
	GENDER (PERSONAL_INFO_KEYS[4], "Gender: Male or Female"),
	ADDRESS_ST_AVENUE (HOME_INFO_KEYS[1], "Gender: Male or Female"),
	ADDRESS_CITY (HOME_INFO_KEYS[2], "Address city"),
	ADDRESS_STATE (HOME_INFO_KEYS[3], "Address State/Province"),
	ADDRESS_ZIP_CODE (HOME_INFO_KEYS[2], "Address ZIP Code");
	
	private String key;
	private String attributeDescription;
	
	PersonAttribute(String key, String attributeDescription){
		this.key = key;
		this.attributeDescription = attributeDescription;
	}
	
	public String getKey(){
		return key;
	}
	
	public String getAttributeDescription(){
		return attributeDescription;
	}
}
