package br.com.med.common.enumeration;

public enum InstitutionalType {
	HOSPITAL,
	CLINICAL,
	INSTITUTION,
	UNIVERSITY,
	VENDOR,
	SOCIETY
}
