package br.com.med.common.data;

import java.io.Serializable;

public class CompanyInfo implements Serializable {

	private static final long serialVersionUID = 4891333109054248130L;
	
	public CompanyInfo() {}
	
	private String companyId;
	private String companyName;
	private String specificBusiness;

	public String getCompanyId() {
		return companyId;
	}
	
	public void setCompanyId(String companyId) {
		this.companyId = companyId;
	}
	
	public String getCompanyName() {
		return companyName;
	}
	
	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}
	
	public String getSpecificBusiness() {
		return specificBusiness;
	}
	
	public void setSpecificBusiness(String specificBusiness) {
		this.specificBusiness = specificBusiness;
	}
	
}
