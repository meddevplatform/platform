package br.com.med.common.account.data;

import br.com.med.common.account.data.InstitutionalProfile.InstitutionalAttribute;
import br.com.med.common.enumeration.MedGroup;
import br.com.med.common.enumeration.MedMembership;

public class AgentProfile extends AbstractPersonProfile{
	
	public AgentProfile(String userName){
		super(userName, MedMembership.MEMBER, MedGroup.AGENT);
	}
	
	public void setInstitutionalId(String institutionalId){
		setAttribute(InstitutionalAttribute.INSTITUTIONAL_ID.getKey(), institutionalId);
	}
	
	public String getInstitutionalId(){
		return getAttribute(InstitutionalAttribute.INSTITUTIONAL_ID.getKey());
	}

}
