package br.com.med.common.data;

import java.io.Serializable;

public class UniversityInfo implements Serializable {

	private static final long serialVersionUID = 6510501846204939240L;
	
	private String universityName;
	private String studentId;
	
	public UniversityInfo() {}

	public String getUniversityName() {
		return universityName;
	}

	public void setUniversityName(String universityName) {
		this.universityName = universityName;
	}

	public String getStudentId() {
		return studentId;
	}

	public void setStudentId(String studentId) {
		this.studentId = studentId;
	}

}
