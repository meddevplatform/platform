package br.com.med.common.enumeration;

public enum InstitutionalAttribute{
	INSTITUTIONAL_NAME ("user.instutitional.name", "Institutional name"),
	INSTITUTIONAL_TYPE ("user.instutitional.type", "Institutional type"),
	INSTITUTIONAL_ID ("user.instutitional.id", "Institutional ID"),
	DIRECTOR_CRM ("user.institutional.director.crm.id", "Director's CRM"),
	BUSINESS_FOCUS ("user.institutional.business", "Business focus of operation");
	
	private String key;
	private String attributeDescription;
	
	InstitutionalAttribute(String key, String attributeDescription){
		this.key = key;
		this.attributeDescription = attributeDescription;
	}
	
	public String getKey(){
		return key;
	}
	
	public String getAttributeDescription(){
		return attributeDescription;
	}
}
