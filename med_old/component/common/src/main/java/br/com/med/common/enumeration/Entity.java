package br.com.med.common.enumeration;

public enum Entity {
	INDIVIDUAL("Pessoa Física"),
	LEGAL("Pessoa Jurídica");
	
	private String displayValue;
	
	private Entity(String displayValue){
		this.displayValue = displayValue;
	}
	
	public String getDisplayValue() {
		return displayValue;
	}
}
