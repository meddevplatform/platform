package br.com.med.common.account.data;

import java.util.HashSet;
import java.util.Set;

import br.com.med.common.enumeration.MedGroup;
import br.com.med.common.enumeration.MedMembership;

public class DoctorProfile extends AbstractPersonProfile{

	// Another attributes are available on User.java!
	public enum DoctorAttribute{
		CRM ("user.doctor.crm.id", "Regional Medici Register"),
		CRM_STATE ("user.doctor.crm.state", "State where CRM was issued"),
		AMBASSOR ("user.doctor.ambassor", "Ambassor that has invited"),
		SPECIALIZATIONS ("user.doctor.specializations", "Specializations");
		
		private String key;
		private String attributeDescription;
		
		DoctorAttribute(String key, String attributeDescription){
			this.key = key;
			this.attributeDescription = attributeDescription;
		}
		
		public String getKey(){
			return key;
		}
		
		public String getAttributeDescription(){
			return attributeDescription;
		}
	}
	
	private Set<String> specializations;
	public static final String SPECIALIZATIONS_SEPARATOR = "\u0026";
	
	public DoctorProfile(String userName){
		super(userName, MedMembership.MEMBER, MedGroup.DOCTOR);
		specializations = new HashSet<String>();
	}
	
	public void setCRM(String crm){
		setAttribute(DoctorAttribute.CRM.getKey(), crm);
	}
	
	public String getCRM(){
		return getAttribute(DoctorAttribute.CRM.getKey());
	}
	
	public void setCRMState(String crmState){
		setAttribute(DoctorAttribute.CRM_STATE.getKey(), crmState);
	}
	
	public String getCRMState(){
		return getAttribute(DoctorAttribute.CRM_STATE.getKey());
	}
	
	public void setAmbassor(String ambassor){
		setAttribute(DoctorAttribute.AMBASSOR.getKey(), ambassor);
	}
	
	public String getAmbassor(){
		return getAttribute(DoctorAttribute.AMBASSOR.getKey());
	}
	
	public void addSpecialization(String specialization){
		Set<String> specializations = getSpecializations();
		if(!specializations.contains(specialization)){
			specializations.add(specialization);
		}
		String valuesConverted = convertSetToString(specializations);
		setAttribute(DoctorAttribute.SPECIALIZATIONS.getKey(), valuesConverted);
	}
	
	public void removeSpecialization(String specialization){
		if(specializations.remove(specialization)){
			String valuesConverted = convertSetToString(specializations);
			setAttribute(DoctorAttribute.SPECIALIZATIONS.toString(), valuesConverted);
		}
	}
	
	public Set<String> getSpecializations(){
		return this.specializations;
	}
	
	
	// TODO: move to a Util class!
	private String convertSetToString(Set<String> setToConvert){
		StringBuilder builder = new StringBuilder();
		for(String value : setToConvert){
			if(builder.length() > 0){
				builder.append(SPECIALIZATIONS_SEPARATOR);
			}
			builder.append(value);
		}
		return builder.toString();
	}
	
	// TODO: move to a Util class and test it!
	private Set<String> convertStringToSet(String stringToConvert){
		Set<String> values = new HashSet<String>();
		int position = stringToConvert.indexOf(SPECIALIZATIONS_SEPARATOR);
		int previousPosition = 0;
		// Trying to avoid Exception on modifying values inside a loop!
		while(position != -1 || position < stringToConvert.length()){
			String value = stringToConvert.substring(previousPosition, position);
			values.add(value);
			previousPosition = position + SPECIALIZATIONS_SEPARATOR.length();
			position = stringToConvert.indexOf(SPECIALIZATIONS_SEPARATOR, previousPosition);
		}
		return values;
	}

}
