package br.com.med.common.enumeration;

public enum DoctorAttribute{
	CRM ("user.doctor.crm.id", "Regional Medici Register"),
	CRM_STATE ("user.doctor.crm.state", "State where CRM was issued"),
	AMBASSOR ("user.doctor.ambassor", "Ambassor that has invited"),
	SPECIALIZATIONS ("user.doctor.specializations", "Specializations");
	
	private String key;
	private String attributeDescription;
	
	DoctorAttribute(String key, String attributeDescription){
		this.key = key;
		this.attributeDescription = attributeDescription;
	}
	
	public String getKey(){
		return key;
	}
	
	public String getAttributeDescription(){
		return attributeDescription;
	}
}
