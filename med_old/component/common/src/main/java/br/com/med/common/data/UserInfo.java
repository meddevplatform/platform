package br.com.med.common.data;

import java.io.Serializable;

import br.com.med.common.enumeration.Entity;
import br.com.med.common.enumeration.MedGroup;

public class UserInfo implements Serializable {

	private static final long serialVersionUID = -2648175993718295731L;
	
	private String username;
	private String password;
	private String confirmPassword;
	
	private String firstName;
	private String lastName;
	private String gender;
	private String email;
	private String birthDate;
	
	private String ambassador;
	
	private Entity entity;
	private MedGroup group;
	
	public String getUsername() {
		return username;
	}
	
	public void setUsername(String username) {
		this.username = username;
	}
	
	public String getPassword() {
		return password;
	}
	
	public void setPassword(String password) {
		this.password = password;
	}
	
	public String getConfirmPassword() {
		return confirmPassword;
	}
	
	public void setConfirmPassword(String confirmPassword) {
		this.confirmPassword = confirmPassword;
	}
	
	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getBirthDate() {
		return birthDate;
	}

	public void setBirthDate(String birthDate) {
		this.birthDate = birthDate;
	}

	public String getAmbassador() {
		return ambassador;
	}
	
	public void setAmbassador(String ambassador) {
		this.ambassador = ambassador;
	}
	
	public Entity getEntity() {
		return entity;
	}
	
	public void setEntity(Entity entity) {
		this.entity = entity;
	}
	
	public MedGroup getGroup() {
		return group;
	}
	
	public void setGroup(MedGroup group) {
		this.group = group;
	}
	
}
