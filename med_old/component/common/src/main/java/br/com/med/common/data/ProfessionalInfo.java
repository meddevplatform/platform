package br.com.med.common.data;

import java.io.Serializable;

public class ProfessionalInfo implements Serializable {

	private static final long serialVersionUID = -2322005126774647591L;

	private String crm;
	private String specializations;
	
	public ProfessionalInfo() {}
	
	public String getCrm() {
		return crm;
	}
	
	public void setCrm(String crm) {
		this.crm = crm;
	}
	
	public String getSpecializations() {
		return specializations;
	}
	
	public void setSpecializations(String specializations) {
		this.specializations = specializations;
	}
	
}
