package br.com.med.common.account.data;

import java.io.Serializable;

public class Address implements Serializable {

	private static final long serialVersionUID = 3279077587396287932L;
	
	private String streetAvenue;
	private String city;
	private String state;
	private String zipCode;
	
	private boolean isEmpty;
	
	public Address() {
		this.isEmpty = true;
	}
	
	public String getStreetAvenue() {
		return streetAvenue;
	}
	
	public void setStreetAvenue(String streetAvenue) {
		this.streetAvenue = streetAvenue;
	}
	
	public String getCity() {
		return city;
	}
	
	public void setCity(String city) {
		this.city = city;
	}
	
	public String getState() {
		return state;
	}
	
	public void setState(String state) {
		this.state = state;
	}
	
	public String getZipCode() {
		return zipCode;
	}
	
	public void setZipCode(String zipCode) {
		this.zipCode = zipCode;
	}

	public boolean isEmpty() {
		return isEmpty;
	}

	public void setEmpty(boolean isEmpty) {
		this.isEmpty = isEmpty;
	}

	@Override
	public String toString() {
		return "Address [streetAvenue=" + streetAvenue + ", city=" + city
				+ ", state=" + state + ", zipCode=" + zipCode + ", isEmpty=" + isEmpty + "]";
	}
	
}
