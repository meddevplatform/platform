package br.com.med.common.account.data;

import br.com.med.common.enumeration.MedGroup;
import br.com.med.common.enumeration.MedMembership;

public class InstitutionalProfile extends AbstractCompanyProfile{
	
	// Another attributes are available on User.java!
	public enum InstitutionalAttribute{
		INSTITUTIONAL_NAME ("user.instutitional.name", "Institutional name"),
		INSTITUTIONAL_TYPE ("user.instutitional.type", "Institutional type"),
		INSTITUTIONAL_ID ("user.instutitional.id", "Institutional ID"),
		DIRECTOR_CRM ("user.institutional.director.crm.id", "Director's CRM"),
		BUSINESS_FOCUS ("user.institutional.business", "Business focus of operation");
		
		private String key;
		private String attributeDescription;
		
		InstitutionalAttribute(String key, String attributeDescription){
			this.key = key;
			this.attributeDescription = attributeDescription;
		}
		
		public String getKey(){
			return key;
		}
		
		public String getAttributeDescription(){
			return attributeDescription;
		}
	}
	
	public InstitutionalProfile(String userName, MedGroup groupId){
		super(userName, MedMembership.MEMBER, groupId);
	}
	
	public void setInstitutionalName(String institutionalName){
		setAttribute(InstitutionalAttribute.INSTITUTIONAL_NAME.getKey(), institutionalName);
	}
	
	public String getInstitutionalName(){
		return getAttribute(InstitutionalAttribute.INSTITUTIONAL_NAME.getKey());
	}
	
	public void setInstitutionalId(String institutionalId){
		setAttribute(InstitutionalAttribute.INSTITUTIONAL_ID.getKey(), institutionalId);
	}
	
	public String getInstitutionalId(){
		return getAttribute(InstitutionalAttribute.INSTITUTIONAL_ID.getKey());
	}
	
	public void setDirectorCRM(String directorCMR){
		setAttribute(InstitutionalAttribute.DIRECTOR_CRM.getKey(), directorCMR);
	}
	
	public String getDirectorCRM(){
		return getAttribute(InstitutionalAttribute.DIRECTOR_CRM.getKey());
	}
	
	public void setBusinessFocus(String businessFocus){
		setAttribute(InstitutionalAttribute.BUSINESS_FOCUS.getKey(), businessFocus);
	}
	
	public String getBusinessFocus(){
		return getAttribute(InstitutionalAttribute.BUSINESS_FOCUS.getKey());
	}

}
